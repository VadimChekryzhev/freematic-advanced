<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */

/* @var $model \frontend\models\SignupForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$this->title = 'Регистрация';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="site-signup">
  <h1><?= Html::encode($this->title) ?></h1>

  <p>Пожалуйста, заполните следующие поля для регистрации:</p>

  <?php if (Yii::$app->session->hasFlash('error_signup')) : ?>
    <div class="alert alert-danger alert-dismissable alert-registr">
      <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
      <?= Yii::$app->session->getFlash('error_user_transfer') ?>
    </div>
  <?php endif; ?>

  <div class="row">
    <div class="col-lg-5">
      <?php $form = ActiveForm::begin(['id' => 'form-signup']); ?>

      <?= $form->field($model, 'username')->textInput(['autofocus' => true])?>

      <?= $form->field($model, 'email') ?>

      <?= $form->field($model, 'password')->passwordInput() ?>

      <div class="form-group">
        <?= Html::submitButton('Signup', ['class' => 'btn btn-primary', 'name' => 'signup-button']) ?>
      </div>

      <?php ActiveForm::end(); ?>
    </div>
  </div>
</div>
