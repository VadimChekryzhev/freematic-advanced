<?php

use yii\helpers\Html;
use common\models\Helpfunc;

?>


<div class="wraper">
  <h2>Личный кабинет</h2>

  <?php

  if (\Yii::$app->user->can('user')) {
    echo "<p> Привет Юзер </p>";
  } elseif (\Yii::$app->user->can('admin'))
  {
    echo "<p> Привет Админ </p>";
  } else {
    echo "<p> Ты не Юзер </p>";
  }


//  $user = \common\models\User::findOne(1);

//  $user = \common\models\User::find()->where(['id' => 1])->all();

//  Helpfunc::debug($user);
//  Helpfunc::debug(count($user[0]->billsfrom));
//  Helpfunc::debug(count($user[0]->billsto));
//  Helpfunc::debug($user[0]->billsfrom);
//  Helpfunc::debug($user[0]->billsto);
//  echo '<pre>'. print_r($user, true) .'</pre>';

  ?>

  <?= Html::a('Просмотреть историю счета', ['/user/history'], ['class' => 'custom-button long-button'])?>
  <?= Html::a('Перевести средства другому пользователю', ['/user/transfer'], ['class' => 'custom-button long-button'])?>
  <?= Html::a('Удалить аккаунт', ['#'], ['class' => 'custom-button long-button js-button-campaign'])?>

</div>

<!--<h2>Личный кабинет</h2>-->