<?php

namespace frontend\controllers;

use common\models\Bills;
use common\models\Helpfunc;
use frontend\models\Balance;
use Yii;
use yii\base\InvalidParamException;
use yii\web\BadRequestHttpException;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use common\models\LoginForm;
use frontend\models\PasswordResetRequestForm;
use frontend\models\ResetPasswordForm;
use frontend\models\SignupForm;
use common\models\User;
use yii\helpers\Url;
use yii\helpers\Html;

/**
 * Site controller
 */
class SiteController extends Controller
{
  /**
   * @inheritdoc
   */
  public function behaviors()
  {
    return [
      'access' => [
        'class' => AccessControl::className(),
        'only' => ['logout', 'signup'],
        'rules' => [
          [
            'actions' => ['signup'],
            'allow' => true,
            'roles' => ['?'],
          ],
          [
            'actions' => ['logout'],
            'allow' => true,
            'roles' => ['@'],
          ],
        ],
      ],
      'verbs' => [
        'class' => VerbFilter::className(),
        'actions' => [
          'logout' => ['post'],
        ],
      ],
    ];
  }

  /**
   * @inheritdoc
   */
  public function actions()
  {
    return [
      'error' => [
        'class' => 'yii\web\ErrorAction',
      ],
      'captcha' => [
        'class' => 'yii\captcha\CaptchaAction',
        'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
      ],
    ];
  }

  /**
   * Displays homepage.
   *
   * @return mixed
   */
  public function actionIndex()
  {
    return $this->render('index');
  }

  /**
   * Logs in a user.
   *
   * @return mixed
   */
  public function actionLogin()
  {
    if (!Yii::$app->user->isGuest) {
      return $this->goHome();
    }

    $model = new LoginForm();
    if ($model->load(Yii::$app->request->post()) && $model->login()) {
      return $this->goBack();
    } else {
      return $this->render('login', [
        'model' => $model,
      ]);
    }
  }

  /**
   * Logs out the current user.
   *
   * @return mixed
   */
  public function actionLogout()
  {
    Yii::$app->user->logout();
    return $this->goHome();
  }

  /**
   * Signs user up.
   *
   * @return mixed
   */


  public function actionSignup()
  {
    if (!Yii::$app->user->isGuest) {
      return $this->goHome();
    }

    $model = new SignupForm();
    if ($model->load(Yii::$app->request->post()) && $model->validate()) {
      if ($user = $model->signup()) {
        $auth = Yii::$app->authManager;
        $userRole = $auth->getRole('user');
        $auth->assign($userRole, $user->getId());
        //***********************************************************************************************
        $absoluteHomeUrl = Url::home(true); //http://ваш сайт
        $serverName = Yii::$app->request->serverName; //ваш сайт без http
        $url = $absoluteHomeUrl . 'activation/' . $user->activation;
        $to = $user->email;
        $subject = "Поздравляем с регистрацией";
        $message = "<p>Поздравляем с регистрацией на сайте $serverName </p>";
        $message .= "<p>Ваш логин: " . $user->username . "</p>";
        $message .= "<p>Ваш пароль: " . $user->password . "</p>";
        $message .= "<p>Для подтверждения регистрации пройдите по ссылке: <a>$url</a></p>";
        $headers = "Content-type: text/html; charset=utf-8 \r\n";
        $headers .= "From: support@" . $serverName . " <support@whoimi.ru>\r\n";
        $headers .= "Reply-To: support@" . $serverName . "\r\n";

        mail($to, $subject, $message, $headers);
        //************************************************************************************************
        return $this->goHome();
      }
    }

    return $this->render('signup', compact('model'));
  }


  /**
   * Requests password reset.
   *
   * @return mixed
   */
  public function actionRequestPasswordReset()
  {
    $model = new PasswordResetRequestForm();
    if ($model->load(Yii::$app->request->post()) && $model->validate()) {
      if ($model->sendEmail()) {
        Yii::$app->session->setFlash('success', 'Check your email for further instructions.');

        return $this->goHome();
      } else {
        Yii::$app->session->setFlash('error', 'Sorry, we are unable to reset password for the provided email address.');
      }
    }

    return $this->render('requestPasswordResetToken', [
      'model' => $model,
    ]);
  }

  /**
   * Resets password.
   *
   * @param string $token
   * @return mixed
   * @throws BadRequestHttpException
   */
  public function actionResetPassword($token)
  {
    try {
      $model = new ResetPasswordForm($token);
    } catch (InvalidParamException $e) {
      throw new BadRequestHttpException($e->getMessage());
    }

    if ($model->load(Yii::$app->request->post()) && $model->validate() && $model->resetPassword()) {
      Yii::$app->session->setFlash('success', 'New password saved.');

      return $this->goHome();
    }

    return $this->render('resetPassword', [
      'model' => $model,
    ]);
  }


  //******************************************************************************************************

  public function actionActivation()
  {
    $code = Yii::$app->request->get('code');
    $code = Html::encode($code);

    $find = User::find()->where(['activation' => $code])->one();

    if ($find) {
      $find->status_email = 1;
      if ($find->save()) {
        $text = '<p>Поздравляю!</p>
          <p>Ваш e-mail успешно подтвержден!</p>';
//        }
        //страница подтверждения
        return $this->render('activation', [
          'text' => $text
        ]);
      }
    }
    $absoluteHomeUrl = Url::home(true);
    return $this->redirect($absoluteHomeUrl, 303); //на главную
  }

}
