<?php
/**
 * Created by PhpStorm.
 * User: chvs
 * Date: 30.10.2017
 * Time: 1:20
 */

namespace frontend\controllers;

use frontend\models\Balance;
use frontend\models\Transfer;
use Yii;
use yii\web\Controller;
use common\models\Bills;
use common\models\Helpfunc;
use common\models\User;

class UserController extends Controller
{

  public function actionIndex()
  {
    return $this->render('index');
  }


  public function actionHistory()
  {
    $query = Bills::find()->where(["user_from" => Yii::$app->user->identity->id])->asArray()->all();

    $active_str = '';

    foreach ($query as $row) {
      $active_str .= '<tr><td>' . $row['user_from'] . '</td><td>' . $row['user_to'] . '</td><td>' . $row['scope'] . '</td><td>' . $row['balance_from'] . '</td></tr>';
    }

    if ($active_str == '') {
      $active_str .= '<tr><td>n/a</td><td>n/a</td><td>n/a</td><td>n/a</td></tr>';
    }

    return $this->render('history', [
      'active_str' => $active_str,
    ]);

  }


  public function actionTransfer()
  {
    $model = new Transfer();

    if ($model->load(Yii::$app->request->post()) && $model->validate()) {
//      не хватает средств для перевода
      if ($model->scope > Yii::$app->user->identity->balance) {
        Yii::$app->session->setFlash('email_to_email_transfer', 'На вашем балансе не хватает средств для перевода');
      } else {
        $user_to = User::findOne(['email' => $model->user_to]);
        $balance_to = Balance::findOne(["id_user" => $user_to->id]);

//      перевод самому себе
        if (Yii::$app->user->identity->email == $user_to->email) {
          Yii::$app->session->setFlash('email_to_email_transfer', 'Вы не можете отправить средства самому себе');
        } else {

//        перевод другому пользователю, если пользователь найден
          if ($user_to) {
            if ($transfer = $model->transfer()) {

              $user_from = Balance::findOne(["id_user" => $transfer->user_from]);
              $user_from->updateBalance(-$transfer->scope);
              $balance_to->updateBalance($transfer->scope);

//          Применение изменений к БД
              $transaction = Yii::$app->db->beginTransaction();
              try {
                if ($transfer->save() && $user_from->save() && $balance_to->save()) {
                  Yii::$app->session->setFlash('success_transfer', 'Перевод стредств выполнен успешно');
                  $transaction->commit();
                }
              } catch (\Throwable $e) {
                Yii::$app->session->setFlash('error_user_transfer', 'Ошибка при изменении балансов пользователей');
                $transaction->rollBack();
              }

            } else {
              Yii::$app->session->setFlash('error_transfer', 'Ошибка при переводе стредств');
            }

          } else {
            Yii::$app->session->setFlash('no_email_transfer', "Пользователь с таким Email не найден");
          }

        }
      }
    }

    return $this->render('transfer', compact("model"));
  }

}