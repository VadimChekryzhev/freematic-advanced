<?php
/**
 * Created by PhpStorm.
 * User: chvs
 * Date: 30.10.2017
 * Time: 3:06
 */

namespace frontend\models;

use common\models\Bills;
use Yii;
use yii\db\ActiveRecord;
use common\models\User;

class Transfer extends ActiveRecord
{

  public static function tableName()
  {
    return 'bills';
  }


  public function attributeLabels()
  {
    return [
      'user_to' => 'Email',
      'scope' => 'Сумма перевода',
    ];
  }

  /**
   * @inheritdoc
   */
  public function rules()
  {
    return [
      ['user_to', 'trim'],
      ['user_to', 'required'],
      ['user_to', 'email'],
      ['user_to', 'string', 'max' => 255],


      ['scope', 'trim'],
      ['scope', 'required'],
      ['scope', 'string', 'min' => 1],
    ];
  }

  public function transfer($autor_id = null, $user_to = null, $user_from = null)
  {
    $new_transfer = new Bills();
//    кому
    $user_to = User::findOne(['email' => $user_to ? $user_to : $this->user_to]);
    $balance_to = $user_to->getBalance();

    $new_transfer->user_from = $user_from ? $user_from : Yii::$app->user->identity->id;
    $new_transfer->balance_from = Yii::$app->user->identity->getBalance() - $new_transfer->scope;
    $new_transfer->scope = $this->scope;
    $new_transfer->user_to = $user_to->id;
    $new_transfer->balance_to = $balance_to + $this->scope;
    $new_transfer->date = date("Y-m-d H:i:s");
    $new_transfer->translator = $autor_id ? $autor_id : $new_transfer->user_from;


//    return $new_transfer->save() ? $new_transfer : null;
    return $new_transfer;
  }

  public function transfer_user($autor_id = null, $user_to, $user_from)
  {
    $new_transfer = new Bills();

    $new_transfer->user_from = $user_from->id;
    $new_transfer->user_to = $user_to->id;

    $new_transfer->balance_from = $user_from->getBalance() - $new_transfer->scope;
    $new_transfer->balance_to = $user_to->getBalance() + $this->scope;

    $new_transfer->scope = $this->scope;
    $new_transfer->date = date("Y-m-d H:i:s");
    $new_transfer->translator = $autor_id ? $autor_id : $new_transfer->user_from;

    return $new_transfer;
  }




}