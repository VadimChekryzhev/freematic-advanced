<?php
/**
 * Created by PhpStorm.
 * User: chvs
 * Date: 30.10.2017
 * Time: 0:51
 */

namespace common\models;


use yii\web\Controller;

class Helpfunc extends Controller
{
  public static function r2f($num) {
    return number_format((float)$num, 2, '.', '');
  }

  public static function debug($arr)
  {
    echo '<pre>'. print_r($arr, true) .'</pre>';

  }

}