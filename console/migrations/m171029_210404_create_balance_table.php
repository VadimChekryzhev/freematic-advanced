<?php

use yii\db\Migration;

/**
 * Handles the creation of table `balance`.
 */
class m171029_210404_create_balance_table extends Migration
{
  /**
   * @inheritdoc
   */
  public function up()
  {
    $this->createTable('balance', [
      'id' => $this->primaryKey(),
      'user' => $this->string()->notNull(),
      'balance' => $this->double(),
    ]);
  }

  /**
   * @inheritdoc
   */
  public function down()
  {
    $this->dropTable('balance');
  }
}
