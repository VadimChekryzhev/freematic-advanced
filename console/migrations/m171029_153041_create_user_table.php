<?php

use yii\db\Migration;

/**
 * Handles the creation of table `user`.
 */
class m171029_153041_create_user_table extends Migration
{
  /**
   * @inheritdoc
   */
  public function up()
  {
    $this->createTable('user', [
      'id' => $this->primaryKey(),
      'username' => $this->string(),
      'email' => $this->string()->defaultValue(0),
      'status_email' => $this->integer()->defaultValue(0),
      'password' => $this->string(),
      'activation' => $this->string()->defaultValue(null),
      'status' => $this->integer()->defaultValue(0),
      'auth_key' => $this->string()->defaultValue(0),
      'created_at' => $this->dateTime(),
      'updated_at' => $this->dateTime(),
    ]);
  }

  /**
   * @inheritdoc
   */
  public function down()
  {
    $this->dropTable('user');
  }
}
