<?php

use yii\db\Migration;

/**
 * Handles the creation of table `bills`.
 */
class m171030_105850_create_bills_table extends Migration
{
  /**
   * @inheritdoc
   */
  public function up()
  {
    $this->createTable('bills', [
      'id' => $this->primaryKey(),
      'email_from' => $this->string()->notNull(),
      'email_to' => $this->string()->notNull(),
      'scope' => $this->double()->notNull(),
      'balance_from' => $this->double()->defaultValue(0),
      'balance_to' => $this->double()->defaultValue(0),
    ]);
  }

  /**
   * @inheritdoc
   */
  public function down()
  {
    $this->dropTable('bills');
  }
}
