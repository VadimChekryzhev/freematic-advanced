<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "user".
 *
 * @property integer $id
 * @property string $username
 * @property string $email
 * @property integer $status_email
 * @property string $password
 * @property string $activation
 * @property integer $status
 * @property string $auth_key
 * @property string $created_at
 * @property string $updated_at
 */
class User extends \yii\db\ActiveRecord
{
  /**
   * @inheritdoc
   */
  public static function tableName()
  {
    return 'user';
  }

  /**
   * @inheritdoc
   */
  public function rules()
  {
    return [
      [['status_email', 'status'], 'integer'],
      [['created_at', 'updated_at'], 'safe'],
      [['username', 'email', 'password', 'activation', 'auth_key'], 'string', 'max' => 255],
    ];
  }

  /**
   * @inheritdoc
   */
  public function attributeLabels()
  {
    return [
      'id' => 'ID',
      'username' => 'Username',
      'email' => 'Email',
      'status_email' => 'Status Email',
      'password' => 'Password',
      'activation' => 'Activation',
      'status' => 'Status',
      'auth_key' => 'Auth Key',
      'created_at' => 'Created At',
      'updated_at' => 'Updated At',
    ];
  }
}
