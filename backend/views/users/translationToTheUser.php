<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */

/* @var $model \frontend\models\SignupForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
$this->title = 'Перевод средств';
$this->params['breadcrumbs'][] = $this->title;
?>

<h2>Зачислить деньги пользователю <?= $username . '('. $email .')' ?></h2>

<?php if (Yii::$app->session->hasFlash('success_transfer')) : ?>
  <div class="alert alert-success alert-dismissable alert-registr">
    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
    <?= Yii::$app->session->getFlash('success_transfer') ?>
  </div>
<?php endif; ?>

<?php if (Yii::$app->session->hasFlash('error_transfer')) : ?>
  <div class="alert alert-danger alert-dismissable alert-registr">
    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
    <?= Yii::$app->session->getFlash('error_transfer') ?>
  </div>
<?php endif; ?>

<div class="row">
  <div class="col-lg-5">
    <?php $form = ActiveForm::begin(['id' => 'form-signup']); ?>

    <?= $form->field($model, 'user_to')->hiddenInput(['value' => $email])->label(false)?>

    <?= $form->field($model, 'scope')->textInput(['autofocus' => true]) ?>

    <div class="form-group">
      <?= Html::submitButton('Подтвердить', ['class' => 'btn btn-primary', 'name' => 'signup-button']) ?>
    </div>

    <?php ActiveForm::end(); ?>
  </div>
</div>

