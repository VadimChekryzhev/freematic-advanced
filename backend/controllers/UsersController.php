<?php

namespace backend\controllers;

use common\models\Bills;
use common\models\Helpfunc;
use frontend\models\Balance;
use frontend\models\SignupForm;
use Yii;
use common\models\User;
use backend\models\UsersSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use frontend\models\Transfer;
use yii\data\Pagination;


/**
 * UsersController implements the CRUD actions for User model.
 */
class UsersController extends Controller
{
  /**
   * @inheritdoc
   */
  public function behaviors()
  {
    return [
      'access' => [
        'class' => AccessControl::className(),
        'rules' => [
          [
            'actions' => [
              'logout',
              'index',
              'create',
              'update',
              'delete',
              'view',
              'pay',
              'send',
            ],
            'allow' => true,
            'roles' => ['admin'],
          ],
        ],
      ],
      'verbs' => [
        'class' => VerbFilter::className(),
        'actions' => [
          'delete' => ['POST'],
        ],
      ],
    ];
  }

  /**
   * Lists all User models.
   * @return mixed
   */
  public function actionIndex()
  {

    $searchModel = new UsersSearch();
    $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

    return $this->render('index', [
      'searchModel' => $searchModel,
      'dataProvider' => $dataProvider,
    ]);
  }

  /**
   * Displays a single User model.
   * @param integer $id
   * @return mixed
   */
  public function actionView($id)
  {

//    $history = Bills::findOne(['user_from' => 1]);

//    Helpfunc::debug($history);
//    Helpfunc::debug($history->emailuser);

    $history = Bills::find()->where(['user_from' => $id])->orWhere(['user_to' => $id])->all();

//    Helpfunc::debug($history[0]->emailuser);
//    Helpfunc::debug($history[0]->emailuser);
    $history[0]->emailuserto;
    $history[0]->emailuserfrom;

    $active_str = '';

    foreach ($history as $row) {
//      Helpfunc::debug($row['emailuser'][0]['email']);
      $active_str .= '<tr><td>' . $row['emailuserfrom'][0]['email'] . '</td><td>' . $row['emailuserto'][0]['email'] . '</td><td>' . $row['scope'] . '</td><td>' . $row['balance_from'] . '</td></tr>';
    }

    if ($active_str == '') {
      $active_str .= '<tr><td>n/a</td><td>n/a</td><td>n/a</td><td>n/a</td></tr>';
    }

    return $this->render('view', [
      'model' => $this->findModel($id),
      'active_str' => $active_str,
    ]);
  }

  /**
   * Creates a new User model.
   * If creation is successful, the browser will be redirected to the 'view' page.
   * @return mixed
   */
  public function actionCreate()
  {
//    $model = new User();

    $model = new SignupForm();

    if ($model->load(Yii::$app->request->post()) && $model->validate()) {
      if ($user = $model->signup(1)) {

        $auth = Yii::$app->authManager;
        $userRole = $auth->getRole('user');
        $auth->assign($userRole, $user->getId());

        $score = new Balance();
        $score->id_user = $user->id;
        $score->balance = 0;
        $score->save();


        return $this->redirect(['/users']);
      } else {
        echo "невозможно добавить пользователя";
      }
    } else {
      return $this->render('create', [
        'model' => $model,
      ]);
    }
  }

  /**
   * Updates an existing User model.
   * If update is successful, the browser will be redirected to the 'view' page.
   * @param integer $id
   * @return mixed
   */
  public function actionUpdate($id)
  {
    $model = $this->findModel($id);

    if ($model->load(Yii::$app->request->post()) && $model->save()) {
      return $this->redirect(['view', 'id' => $model->id]);
    } else {
      return $this->render('update', [
        'model' => $model,
      ]);
    }
  }

  /**
   * Deletes an existing User model.
   * If deletion is successful, the browser will be redirected to the 'index' page.
   * @param integer $id
   * @return mixed
   */
  public function actionDelete($id)
  {
    $this->findModel($id)->delete();

    return $this->redirect(['index']);
  }


  public function actionPay($id)
  {
    $user = $this->findModel($id);
    $model = new Transfer();

    if ($model->load(Yii::$app->request->post()) && $model->validate()) {

      $user_to = User::findOne(['email' => $model->user_to]);
      $user_from = User::findOne(Yii::$app->user->id);

//      не хватает средств для перевода
      if ($model->scope > Yii::$app->user->identity->balance) {
        Yii::$app->session->setFlash('email_to_email_transfer', 'На вашем балансе не хватает средств для перевода');
      } elseif (Yii::$app->user->identity->email == $user_to->email) {
        Yii::$app->session->setFlash('email_to_email_transfer', 'Вы не можете отправить средства самому себе');
      } elseif ($user_to) {
        if ($transfer = $model->transfer_user(null, $user_to, $user_from)) {
          $transaction = Yii::$app->db->beginTransaction();
          try {
            if ($transfer->save() && $user_from->updateBalance(-$transfer->scope) && $user_to->updateBalance($transfer->scope)) {
              Yii::$app->session->setFlash('success_transfer', 'Перевод стредств выполнен успешно');
              $transaction->commit();
              $this->refresh();
            }
          } catch (\Throwable $e) {
            Yii::$app->session->setFlash('error_transfer', 'Ошибка при изменении балансов пользователей');
            $transaction->rollBack();
          }

        } else {
          Yii::$app->session->setFlash('error_transfer', 'Ошибка при переводе стредств');
        }
      } else {
        Yii::$app->session->setFlash('error_transfer', "Пользователь с таким Email не найден");
      }
    }

    $username = $user->username;
    $email = $user->email;

    return $this->render('translationToTheUser', compact('username', 'email', 'model'));
  }


  public function actionSend($id)
  {
    $user = $this->findModel($id);
    $model = new Transfer();

    if ($model->load(Yii::$app->request->post()) && $model->validate()) {

      $user_from = User::findOne($id);
      $user_to = User::findOne(['email' => $model->user_to]);

      if ($model->scope > $user_from->getBalance()) {
        Yii::$app->session->setFlash('error_transfer', 'На балансе пользователя не хватает средств для перевода');
      } elseif ($user_to->email == $user_from->email) {
        Yii::$app->session->setFlash('error_transfer', 'Адрес отправления совпадает с адресом получения');
      } elseif ($user_to && $user_from) {
        if ($transfer = $model->transfer_user(Yii::$app->user->id, $user_to, $user_from)) {

          $transaction = Yii::$app->db->beginTransaction();
          try {
            if ($transfer->save() && $user_from->updateBalance(-$model->scope) && $user_to->updateBalance($model->scope)) {
              Yii::$app->session->setFlash('success_transfer', 'Перевод стредств выполнен успешно');

              $transaction->commit();
              $this->refresh();
            }
          } catch (\Throwable $e) {
            Yii::$app->session->setFlash('error_transfer', 'Ошибка при изменении балансов пользователей');
            $transaction->rollBack();
          }

        } else {
          Yii::$app->session->setFlash('error_transfer', 'Ошибка при переводе стредств');
        }
      } else {
        Yii::$app->session->setFlash('error_transfer', 'Невозможно найти отправителя/получателя');
      }

    }

    $username = $user->username;
    $email = $user->email;

    return $this->render('send', compact('username', 'email', 'model'));
  }

  /**
   * Finds the User model based on its primary key value.
   * If the model is not found, a 404 HTTP exception will be thrown.
   * @param integer $id
   * @return User the loaded model
   * @throws NotFoundHttpException if the model cannot be found
   */
  protected
  function findModel($id)
  {
    if (($model = User::findOne($id)) !== null) {
      return $model;
    } else {
      throw new NotFoundHttpException('The requested page does not exist.');
    }
  }
}
